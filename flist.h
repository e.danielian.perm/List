class ForwardList
{
public:
	ForwardList();
	~ForwardList();
	int GetSize() { return size; }
	void Print();
	void Add(int );
private:
	class Node
	{
	public:
		int data;
		Node* next;
		Node(int data = 0, Node*ptr = nullptr)
		{
			this->data = data;
			this->next = ptr;
		}
	};
	int size;
	Node*first;
};