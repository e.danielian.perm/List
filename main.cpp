#include "flist.h"
#include <iostream>
#include <string>
using namespace std;

void PrintMenu();
void Init(ForwardList* list, int argc, char*argv[]);
void ClearBuffer();

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");
	ForwardList list;
	Init(&list, argc, argv);
	do
	{
		PrintMenu();
		int op;
		while(!(cin >> op) || op <= 0 || op > 7)
		{
			ClearBuffer();
			cout << "Введите целое число от 1 до  7" << endl;
		}
		switch (op)
		{
		case 1: list.Print(); break;
		case 2: 
		{
			string s;
			cout << "Введите элементы:" << endl;
			ClearBuffer();
			getline(cin, s);
			ClearBuffer();
			while (s.find(' ', s.length() - 1) != -1) s.erase(s.length() - 1, 1);
			int k = s.find(' ');
			while (k != -1)
			{
				string str = s.substr(0, k);
				list.Add(atoi(str.c_str()));
				s.erase(0, k + 1);
				k = s.find(' ');
			}
			list.Add(atoi(s.c_str()));
			break;
		}
		case 7: 
			cout << "Вы действительно хотите выйти из программы? (y/n) ";
			string ans;
			while (true)
			{
				ClearBuffer();
				getline(cin, ans);
				ClearBuffer();
				if (ans == "y" || ans == "Y" || ans == "Yes" || ans == "yes" || ans == "YES")
				{
					cout << endl << "До свидания!";
					return 0;
				}
				else
				{
					if (ans == "n" || ans == "N" || ans == "No" || ans == "no" || ans == "NO")
						break;
					else
					{
						cout << "Введите допустимый ответ: ";
						continue;
					}

				}
			}
		}

	} while (true);
}

void PrintMenu()
{
	cout << "Выберите одну из операций :" << endl;
	cout << "1. Распечатать список" << endl;
	cout << "2. Добавить элементы в список" << endl;
	cout << "3. Удалить элемент" << endl;
	cout << "4. Найти позиции элементов" << endl;
	cout << "5. Заменить элемент на другой" << endl;
	cout << "6. Отсортировать элементы списка" << endl;
	cout << "7. Завершить работу программы" << endl;
}

void Init(ForwardList * list, int argc, char*argv[])
{
	if (argc == 1)
	{
		list = new ForwardList();
		return;
	}
	string s(argv[1]);
	if (s.find(',') == -1)
		for (int i = 1; i < argc; i++)
			list->Add(atoi(argv[i]));
	else
	{
		int k = s.find(',');
		while (k != -1)
		{
			string str = s.substr(0, k);
			list->Add(atoi(str.c_str()));
			s.erase(0, k + 1);
			k = s.find(',');
		}
		list->Add(atoi(s.c_str()));
	}
}

void ClearBuffer()
{
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
}
