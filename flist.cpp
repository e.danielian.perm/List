#include "flist.h"
#include <iostream>
using namespace std;

ForwardList::ForwardList()
{
	first = nullptr;
	size = 0;
}

ForwardList::~ForwardList()
{
	Node*cur1 = first;
	while (cur1 != nullptr)
	{
		Node*cur2 = cur1->next;
		delete cur1;
		cur1 = cur2;
	}
}

void ForwardList::Print()
{
	Node* cur = first;
	if (cur == nullptr)
	{
		cout << "Список пустой" << endl;
		return;
	}
	cout << cur->data;
	cur = cur->next;
	while (cur != nullptr)
	{
		cout << " -> " << cur->data;
		cur = cur->next;
	}
	cout << endl;
}

void ForwardList::Add(int x)
{
	size++;
	if (first == nullptr)
		first = new Node(x);
	else
	{
		Node* cur = first;
		while (cur->next != nullptr) cur = cur->next;
		cur->next = new Node(x);
	}
}

